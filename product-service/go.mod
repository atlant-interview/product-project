module gitlab.com/atlant-interview/product-service

go 1.14

require (
	github.com/golang/protobuf v1.5.2
	github.com/micro/go-micro/v2 v2.9.1
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.5.2
	go.uber.org/fx v1.13.1
	google.golang.org/grpc v1.37.1
	google.golang.org/protobuf v1.26.0
)
