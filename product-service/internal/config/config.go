package config

import (
	"strings"

	"github.com/spf13/viper"
)

type DBConfig struct {
	Host string
	Port string
}

type ServiceConfig struct {
	Name string
	Host string
	Port string
}
type DialConfig struct {
	Host string
	Port string
}

// Config is the main struct for config collections and storage
type Config struct {
	DB      DBConfig
	Service ServiceConfig
	Dial    DialConfig
}

// NewConfig constructs configuration as a fx provider
func NewConfig() *Config {
	confer := viper.New()
	confer.AutomaticEnv()
	confer.SetEnvPrefix("PRODUCT")
	confer.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	c := &Config{
		DB: DBConfig{
			Host: confer.GetString("db.host"),
			Port: confer.GetString("db.port"),
		},
		Service: ServiceConfig{
			Name: confer.GetString("service.name"),
			Host: confer.GetString("service.host"),
			Port: confer.GetString("service.port"),
		},
		Dial: DialConfig{
			Host: confer.GetString("dial.host"),
			Port: confer.GetString("dial.port"),
		},
	}

	return c
}
