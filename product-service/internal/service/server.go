package service

import (
	"context"
	"fmt"
	"log"
	"net"

	"gitlab.com/atlant-interview/product-service/internal/config"
	"go.uber.org/fx"
	"google.golang.org/grpc"
)

// NewService create new GRPC service
func NewService(lc fx.Lifecycle, conf *config.Config, handler *ProductHandler) (*grpc.Server, error) {
	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%s", conf.Service.Host, conf.Service.Port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
		return nil, err
	}
	var opts []grpc.ServerOption
	srv := grpc.NewServer(opts...)

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			go func() {
				srv.Serve(lis)
			}()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			return nil
		},
	})
	return srv, nil
}
