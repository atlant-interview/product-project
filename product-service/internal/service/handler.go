package service

import (
	"context"
	"log"
	"strconv"

	"gitlab.com/atlant-interview/product-service/internal/config"
	"gitlab.com/atlant-interview/product-service/internal/usecase"
	pb "gitlab.com/atlant-interview/product-service/pkg/proto"
	"google.golang.org/grpc"
)

type ProductHandler struct {
	pb.UnimplementedProductServer

	config  *config.Config
	usecase usecase.ServerUsecase
}

func NewProductHandler(conf *config.Config, usecase usecase.ServerUsecase) *ProductHandler {
	return &ProductHandler{
		config:  conf,
		usecase: usecase,
	}
}

func RegisterHandlers(srv *grpc.Server, handler *ProductHandler) {
	pb.RegisterProductServer(srv, handler)
}

func (h *ProductHandler) Fetch(ctx context.Context, req *pb.FetchRequest) (*pb.FetchResponse, error) {
	log.Println(h.config.Service.Name, "Server Name")

	_, err := h.usecase.Fetch(ctx, req.Url)
	if err != nil {
		log.Println("Cannot fetch products:", err)
		return nil, err
	}

	return &pb.FetchResponse{
		Message: "Successfully fetched",
	}, nil
}

func (h *ProductHandler) List(ctx context.Context, req *pb.ListRequest) (*pb.ListResponse, error) {
	products, _, err := h.usecase.List(ctx, req.Cursor, req.Limit)
	if err != nil {
		log.Printf("%+v", products)
		return nil, err
	}
	log.Printf("%+v", products)

	list := make([]*pb.Product, len(products))
	for i, product := range products {
		list[i] = &pb.Product{
			Name:        product.Name,
			Price:       product.Price,
			UpdateCount: strconv.Itoa(product.UpdateCount),
		}
	}

	res := &pb.ListResponse{
		Products: list,
	}
	return res, nil
}
