// db contains database client and migration aa
package mongodb

import (

	//log "github.com/micro/go-micro/v2/logger"

	"context"
	"fmt"
	"log"

	"gitlab.com/atlant-interview/product-service/internal/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/fx"
)

func newMongoDBClient(conf *config.Config) (*mongo.Client, error) {
	var ctx = context.Background()
	uri := fmt.Sprintf("%s:%s/", conf.DB.Host, conf.DB.Port)
	clientOptions := options.Client().ApplyURI(uri)
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}
	return client, nil
}

// NewMongoDBClientProvider provides mongodb collection
func NewMongoDBClientProvider(lc fx.Lifecycle, conf *config.Config) (*mongo.Collection, error) {
	client, err := newMongoDBClient(conf)
	if err != nil {
		log.Print("Database connection error", err)
	}
	var collection *mongo.Collection
	collection = Migrate(client)

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			log.Print("Database Migrated")
			return nil
		},
		OnStop: func(ctx context.Context) error {
			err = client.Disconnect(ctx)
			if err != nil {
				log.Fatal(err)
				return err
			}
			return nil
		},
	})

	return collection, nil
}
