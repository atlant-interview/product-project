package mongodb

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/atlant-interview/product-service/internal/usecase"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoDBRepository is repository to interact with mongo client
type MongoDBRepository struct {
	C *mongo.Collection
}

// NewMongoDBRepository creates new Repository instance
func NewMongoDBRepository(C *mongo.Collection) usecase.ServerRepository {
	return &MongoDBRepository{
		C: C,
	}
}

func (m *MongoDBRepository) InsertManyProducts(ctx context.Context, products []interface{}) error {
	// Delete all
	m.C.DeleteMany(ctx, bson.D{})
	// Insert
	_, err := m.C.InsertMany(ctx, products, &options.InsertManyOptions{})
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

// List all products
func (m *MongoDBRepository) List(ctx context.Context, cursor string, limit int64) ([]*usecase.Product, string, error) {
	p := []*usecase.Product{}
	objID, err := primitive.ObjectIDFromHex(cursor)
	if err != nil {
		log.Println("Cursor error", err)
		return nil, "", err
	}
	opts := &options.FindOptions{
		Limit: &limit,
	}
	res, err := m.C.Find(ctx, bson.M{"_id": bson.M{"$gt": objID}}, opts)
	if err != nil {
		return nil, "", err
	}
	err = res.All(ctx, &p)
	if err != nil {
		return nil, "", err
	}

	return p, "", nil
}
