package usecase

import (
	"context"
	"fmt"
	"time"

	"github.com/micro/go-micro/v2/errors"
	"gitlab.com/atlant-interview/product-service/internal/utils"
)

// Model common fields
type Model struct {
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}

// Product model
type Product struct {
	Name        string `bson:"name,omitempty"`
	Price       string `bson:"price,omitempty"`
	UpdateCount int    `bson:"update_count"`
	Model
}

type ServerUsecase interface {
	Fetch(ctx context.Context, url string) ([][]string, error)
	List(ctx context.Context, cursor string, limit int64) ([]*Product, string, error)
}

type ServerRepository interface {
	InsertManyProducts(ctx context.Context, products []interface{}) error
	List(ctx context.Context, cursor string, limit int64) (res []*Product, nextCursor string, err error)
}

type serverUsecase struct {
	Repo ServerRepository
}

func NewServerUsecase(serverRepo ServerRepository) ServerUsecase {
	return &serverUsecase{Repo: serverRepo}
}

func (u *serverUsecase) Fetch(ctx context.Context, url string) ([][]string, error) {
	csv, err := utils.ReadCSVFromUrl(url)
	if err != nil {
		return nil, errors.InternalServerError("products.service", fmt.Sprintf("Cannot read csv file from URL: %s", url))
	}

	var uniqueProducts = make(map[string]*Product)
	for idx, row := range csv {
		// skip header
		if idx == 0 {
			continue
		}

		// Check if product already exists
		if p, ok := uniqueProducts[row[0]]; ok {
			uniqueProducts[row[0]] = &Product{
				Name:        row[0],
				Price:       row[1],
				UpdateCount: p.UpdateCount + 1,
			}
		} else {
			uniqueProducts[row[0]] = &Product{
				Name:        row[0],
				Price:       row[1],
				UpdateCount: 0,
			}
		}

	}

	var products []interface{}
	for _, v := range uniqueProducts {
		products = append(products, v)
	}

	err = u.Repo.InsertManyProducts(ctx, products)
	if err != nil {
		fmt.Println("Cannot insert products to db", err)
	}

	return csv, nil
}

func (u *serverUsecase) List(ctx context.Context, cursor string, limit int64) ([]*Product, string, error) {

	list, nextCursor, err := u.Repo.List(ctx, cursor, limit)
	//fill this part
	return list, nextCursor, err
}
