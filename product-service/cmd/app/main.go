package main

import (
	"gitlab.com/atlant-interview/product-service/internal/config"
	"gitlab.com/atlant-interview/product-service/internal/db/repository/mongodb"
	"gitlab.com/atlant-interview/product-service/internal/service"
	"gitlab.com/atlant-interview/product-service/internal/usecase"
	"go.uber.org/fx"
)

func main() {

	app := fx.New(
		fx.Provide(
			config.NewConfig,
			service.NewService,
			mongodb.NewMongoDBClientProvider,
			mongodb.NewMongoDBRepository,
			usecase.NewServerUsecase,
			service.NewProductHandler,
		),
		fx.Invoke(
			service.RegisterHandlers,
		),
	)

	app.Run()
}
