package main

import (
	"gitlab.com/atlant-interview/product-service/internal/config"
	"gitlab.com/atlant-interview/product-service/pkg/client"
	"go.uber.org/fx"
)

func main() {
	app := fx.New(
		fx.Provide(
			config.NewConfig,
			client.NewClient,
		),
		fx.Invoke(
			client.FetchCSV,
		),
	)

	app.Run()
}
