package client

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/atlant-interview/product-service/internal/config"
	pb "gitlab.com/atlant-interview/product-service/pkg/proto"
	"google.golang.org/grpc"
)

func NewClient(conf *config.Config) (pb.ProductClient, error) {
	var conn *grpc.ClientConn
	conn, err := grpc.Dial(fmt.Sprintf("%s:%s", conf.Dial.Host, conf.Dial.Port), grpc.WithInsecure())
	if err != nil {
		log.Println("Connection error: ", err.Error())
		return nil, err
	}
	log.Println("Connection created")

	client := pb.NewProductClient(conn)

	return client, nil
}

func FetchCSV(c pb.ProductClient) error {
	req := &pb.FetchRequest{
		Url: "http://localhost:81/convertcsv.csv",
	}

	res, err := c.Fetch(context.Background(), req)
	if err != nil {
		fmt.Printf("error jdncdjncdjncjdncjdjncjdncjdncjn %+v\n", err)
		return err
	}

	fmt.Printf("response %+v\n", res)
	return nil
}
